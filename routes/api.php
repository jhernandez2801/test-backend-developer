<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Register\RegisterController;
use App\Http\Controllers\Password\ForgotPasswordController;
use App\Http\Controllers\Password\ResetPasswordController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Product\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
  'prefix' => 'v1',
  'middleware' => 'api'
], function ($router) {
  //Auth
  Route::post('login', [AuthController::class, 'login']);
  Route::post('logout', [AuthController::class, 'logout']);
  Route::post('refresh', [AuthController::class, 'refresh']);

  //Register
  Route::post('register', [RegisterController::class, 'store']);

  //ForgotPassword
  Route::post('forgot-password', [ForgotPasswordController::class, 'store']);

  //ResetPassword
  Route::post('reset-password', [ResetPasswordController::class, 'store']);

  //CRUD Users
  Route::apiResource('users', UserController::class);

  //CRUD Products
  Route::apiResource('products', ProductController::class);
});
