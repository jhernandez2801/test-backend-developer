<?php
return [
  /**
   * Number of elementos to paginate
   */
  'paginate' => 10,
  /**
   * Number to use into process Storage
   */
  'disk' => 'public'
];