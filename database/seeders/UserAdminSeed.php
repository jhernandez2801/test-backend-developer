<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class UserAdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
          'name' => 'Jorge Hernández',
          'phone' => '61243139',
          'username' => 'jhernandez',
          'birthday' => Carbon::create(1994, 1, 28),
          'email' => 'jjhernandez2801@gmail.com',
          'email_verified_at' => now(),
          'password' => Hash::make('admin123456'), // password
          'remember_token' => Str::random(10),
      ]);
    }
}
