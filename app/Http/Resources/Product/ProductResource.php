<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'sku' => $this->sku,
          'name' => $this->name,
          'quantity' => $this->quantity,
          'price' => $this->price,
          'description' => $this->description,
          'image' => $this->image,
          'image_path' => $this->image_path
        ];
    }
}
