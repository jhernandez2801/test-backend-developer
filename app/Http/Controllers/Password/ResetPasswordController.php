<?php

namespace App\Http\Controllers\Password;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use App\Http\Requests\Password\ResetPasswordStore;
use Illuminate\Validation\ValidationException;

class ResetPasswordController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResetPasswordStore $request)
    {
      $status = Password::reset(
          $request->only('email', 'password', 'password_confirmation', 'token'),
          function ($user, $password) use ($request) {
              $user->forceFill([
                  'password' => Hash::make($password)
              ])->save();

              $user->setRememberToken(Str::random(60));

              event(new PasswordReset($user));
          }
      );

      if($status == Password::PASSWORD_RESET) {
        return response()->json(__($status), 200);
      } else {
        throw ValidationException::withMessages([
          'email' => __($status),
        ]);
      }
    }
}
