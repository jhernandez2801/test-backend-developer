<?php

namespace App\Http\Controllers\Password;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Http\Requests\Password\ForgotPasswordStore;
use Illuminate\Validation\ValidationException;

class ForgotPasswordController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ForgotPasswordStore $request)
    {
        $status = Password::sendResetLink(
          $request->only('email')
        );

        if($status === Password::RESET_LINK_SENT) {
          return response()->json(__($status), 200);
        }
        else {
          throw ValidationException::withMessages([
            'email' => __($status),
          ]);
        }
    }
}
