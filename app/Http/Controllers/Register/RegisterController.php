<?php

namespace App\Http\Controllers\Register;

use App\Http\Controllers\Controller;
use App\Http\Requests\Register\RegisterStore;
use App\Models\User;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterStore $request)
    {
        $user = User::create($request->all());

        return response()->json($user, 201);
    }
}
