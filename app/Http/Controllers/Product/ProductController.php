<?php

namespace App\Http\Controllers\Product;

use App\Models\Product;

use App\Http\Controllers\Controller;

use App\Http\Requests\Product\ProductStore;
use App\Http\Requests\Product\ProductUpdate;
use Illuminate\Http\Request;

use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\ProductResource;

use Illuminate\Support\Facades\Storage;
class ProductController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate = config('products.paginate');

        $sku = $request->sku;

        $name = $request->name;

        return new ProductCollection(
          Product::sku($sku)
            ->name($name)
            ->paginate($paginate)
        );  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Product\ProductStore  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStore $request)
    {
        $product = Product::create($request->except(['image']));

        if($request->file('image')) {
          $disk = config('products.disk');
          $path = Storage::disk($disk)->put('images/products', $request->file('image'));
          $product->image = $path;
          $product->save();
        }

        return response()->json($product, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product = new ProductResource($product);

        return response()->json($product, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Product\ProductUpdate  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdate $request, Product $product)
    {
        $product->update($request->except(['image']));

        if($request->file('image')) {
          $disk = config('products.disk');
          Storage::disk($disk)->delete($product->image);
          $path = Storage::disk($disk)->put('images/products', $request->file('image'));
          $product->image = $path;
          $product->save();
        }

        return response()->json($product, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $disk = config('products.disk');
        Storage::disk($disk)->delete($product->image);
        $product->delete();

        return response()->json(null, 204);
    }
}
