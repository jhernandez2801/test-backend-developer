<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class ProductUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'sku' => [
            'nullable',
            Rule::unique('products')->ignore($this->product),
            'max:255'
          ],
          'name' => [
            'required',
            Rule::unique('products')->ignore($this->product),
            'max:255',
          ],
          'quantity' => [
            'required',
            'integer',
            'regex:/^[0-9]*$/'
          ],
          'price' => [
            'required',
            'numeric',
            'regex:/^\d{1,6}(\.\d{1,2})?$/'
          ],
          'description' => 'nullable',
          'image' => 'nullable|image'
        ];
    }
}
