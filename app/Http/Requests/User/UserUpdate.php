<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'required|max:255',
          'phone' => 'required|numeric',
          'username' => [
            'required',
            Rule::unique('users')->ignore($this->user),
            'max:255'
          ],
          'birthday' => 'required|date',
          'email' => [
            'required',
            'email',
            Rule::unique('users')->ignore($this->user),
            ],
          'password' => 'required|min:8|max:255'
        ];
    }
}
