<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'sku', 'name', 'quantity', 'price', 'description', 'image'
    ];

    /**
     * Filter to search by sku
     */
    public function scopeSku($query, $sku){
      if($sku) {
        $query->where('sku', 'ilike', "%$sku%");
      }
    }

    /**
     * Filter to search by name
     */
    public function scopeName($query, $name){
      if($name) {
        $query->where('name', 'ilike', "%$name%");
      }
    }

    /**
     * Get full path image
     *
     * @param  string  $value
     * @return string
     */
    public function getImagePathAttribute()
    {
        return ($this->image) ? asset(Storage::url($this->image)) : null;
    }
}
